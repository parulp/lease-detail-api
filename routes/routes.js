var appRouter = function (app) {
    
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });

    app.get("/v1/leases", function (req, res) {
        var x = [
                 { "id": "lease-a", "tenant": "Alex" }, 
                 { "id": "lease-b", "tenant": "Jen" }, 
                 { "id": "lease-c", "tenant": "Frankie" }
                ];
        res.send(x);
    });

    app.get("/v1/leases/:id", function (req, res) {
        var id = req.params.id;
        var myArray = [
            {   
                "id": "dummy",
                "start_date": "2018-02-15",
                "end_date": "2018-07-15",
                "rent": 820,
                "frequency": "monthly",
                "payment_day": "friday"
            },
            {
                "id": "dummy",
                "start_date": "2018-05-12",
                "end_date": "2018-11-13",
                "rent": 454,
                "frequency": "weekly",
                "payment_day": "tuesday"
            },
            {
                "id": "dummy",
                "start_date": "2018-07-01",
                "end_date": "2018-12-01",
                "rent": 820,
                "frequency": "fortnightly",
                "payment_day": "wednesday"
            }];

            var x = myArray[Math.floor(Math.random() * myArray.length)];
            x.id = id;
        res.send(x);
    });

}

module.exports = appRouter;